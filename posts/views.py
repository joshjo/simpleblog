from django.shortcuts import render
from .models import Post


def all(request):
    posts = Post.objects.all().order_by('date')
    return render(
        request,
        'posts/all.html',
        {'posts': posts}
    )


def specific(request, slug):
    post = Post.objects.filter(slug=slug).first()
    print "estoy en specific"
    return render(
        request,
        'posts/specific.html',
        {'post': post}
    )
