from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns(
    '',
    url(r'^$', views.all, name='all'),
    url(r'^(?P<slug>[\w-]+)/$', views.specific, name='specific'),
)
