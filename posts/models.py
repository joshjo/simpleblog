from django.db import models


class Post(models.Model):
    STATUS_CHOICES = (
        ('p', 'published'),
        ('u', 'unpublished')
    )
    title = models.CharField(max_length=200)
    body = models.TextField()
    slug = models.SlugField()
    date = models.DateTimeField(auto_now_add=True, blank=True)
    status = models.CharField(
        max_length=1, choices=STATUS_CHOICES, default='u')
