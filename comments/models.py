from django.db import models
from posts.models import Post


class Comment(models.Model):
    body = models.TextField()
    date = models.DateTimeField(auto_now_add=True, blank=True)
#    post = models.ForeignKey('posts.Post')
    post = models.ForeignKey(Post)
